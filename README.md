# trellis-nethack-hid #
This is a quick implementation of HID (Human Interface Device) using the [TrellisM4Express](https://www.adafruit.com/product/3938) from [Adafruilt](http://adafruit.com)

This work is loosely based on [launch-deck-trellis-m4](https://learn.adafruit.com/launch-deck-trellis-m4/code-with-circuitpython).  I've tried to encapsulate things a bit and provide a way to implement a sub-class containing the key code and color definitions.

Certianly open to suggestions on how to enhance and extend.

## Depends on packages ##
These packages must be loaded onto the trellism4 in the 'lib' directory
see https://github.com/adafruit/Adafruit_CircuitPython_Bundle/releases/


  * adafruilt_hid
  * adafruit_trellism4
  * neopixel
  * adafruit_matrixkeypad

## details ##
More details can be found in package documentation

`pydoc trelliskey.tkeyboard'`

`pydoc trelliskey.nethackkeyboard`
