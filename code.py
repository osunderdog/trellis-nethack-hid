
# -*-mode:python; mode:sensitive; fill-column:80-*-

import time
import adafruit_trellism4
from adafruit_hid.keyboard import Keyboard
from adafruit_hid.keycode import Keycode

from trelliskey.nethackKeyboard import nethackKeyboard

ROTATION=180
TIMEOUT=90

kbd = Keyboard()
trellis = adafruit_trellism4.TrellisM4Express(rotation=ROTATION)
#TODO add a blink code here to indicate that the code has been loaded successfully.

kbdef = nethackKeyboard(kbd,trellis)
kbdef.blinkcode((1,1),0x030000)

##what keys are currently pressed.
current_pressed = set()

last_press = time.monotonic()
while True:
    pressed = kbdef.pressed_keys()
    
    now = time.monotonic()
    sleep_time = now - last_press
    sleeping = sleep_time > TIMEOUT

    ##set difference: keys pressed newly.
    kbdef.do_downaction_set(pressed - current_pressed)
    kbdef.do_upaction_set(current_pressed - pressed)
        
    last_press = time.monotonic()
    current_pressed = pressed
