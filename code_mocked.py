# -*-mode:python; mode:sensitive; fill-column:80-*-

import trelliskey.time_mock as time

# import adafruit_trellism4
import trelliskey.trellis_mock as adafruit_trellism4

# from adafruit_hid.keyboard import Keyboard
from trelliskey.keyboard_mock import Keyboard

# from adafruit_hid.keycode import Keycode
from trelliskey.keycode import Keycode
from trelliskey.nethackkeyboard import nethackKeyboard

def blinkcode(t, key):
    for _ in range(10):
        before = t.pixels[key]
        t.pixels[key] = 0xf0f0f0
        time.sleep(1)
        t.pixels[key] = before
        time.sleep(1)


ROTATION=180
TIMEOUT=90

kbd = Keyboard()
trellis = adafruit_trellism4.TrellisM4Express(rotation=ROTATION)

kbdef = nethackKeyboard(kbd,trellis)
kbdef.blinkcode((1,1),0x030000)



##what keys are currently pressed.
current_pressed = set()

last_press = time.monotonic()
while True:
    pressed = kbdef.pressed_keys()
    
    now = time.monotonic()
    sleep_time = now - last_press
    sleeping = sleep_time > TIMEOUT

    ##set difference: keys pressed newly.
    kbdef.do_downaction_set(pressed - current_pressed)
    kbdef.do_upaction_set(current_pressed - pressed)
        
    last_press = time.monotonic()
    current_pressed = pressed
