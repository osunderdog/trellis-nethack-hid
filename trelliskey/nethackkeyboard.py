from trelliskey.keycode import Keycode
from trelliskey.tkeyboard import button, tkeyboard

class nethackKeyboard(tkeyboard):
    """
    Subclass of tkeyboard. Defines key mapping for nethack.
    """
    Keymap =  {
        #direction cross
        #(column,row)
        (1,0): button(0x0a0000,0x0a000f,'North',(Keycode.EIGHT,)),
        (0,1): button(0x0a0000,0x0a000f,'West',(Keycode.FOUR,)),
        (2,1): button(0x0a0000,0x0a000f,'East',(Keycode.SIX,)),
        (1,2): button(0x0a0000,0x0a000f,'South',(Keycode.TWO,)),

        #diag directions
        (0,0): button(0x040000,0x04000f,'NorthWest',(Keycode.SEVEN,)),
        (2,0): button(0x040000,0x04000f,'NorthEast',(Keycode.NINE,)),
        (0,2): button(0x040000,0x04000f,'SouthWest',(Keycode.ONE,)),
        (2,2): button(0x040000,0x04000f,'SouthEast',(Keycode.THREE,)),
        
        #center key
        (1,1): button(0x020200,0x02020f,'Center',(Keycode.FIVE,)),
        
        #idle actions
        (0,3): button(0x000303,0x0f0303,'search',(Keycode.S,)),
        (1,3): button(0x000606,0x0f0606,'pause',(Keycode.PERIOD,)),
        (2,3): button(0x000909,0x0f0909,'space',(Keycode.SPACEBAR,)),
        
        #range attack actions
        (3,0): button(0x040300,0x04030f,'fire',(Keycode.F,)),
        (3,1): button(0x040600,0x04060f,'throw',(Keycode.T,)),
        (3,2): button(0x040900,0x04090f,'zap-wand',(Keycode.Z,)),
        (3,3): button(0x040900,0x04090f,'zap-spell',(Keycode.SHIFT, Keycode.Z,)),
        
        #selection choices
        (4,0): button(0x04030f,0x08030f,'option a',(Keycode.A,)),
        (4,1): button(0x04030f,0x08030f,'option b',(Keycode.B,)),
        (4,2): button(0x04030f,0x08030f,'option c',(Keycode.C,)),
        
        (5,0): button(0x04030f,0x08030f,'option e',(Keycode.D,)),
        (5,1): button(0x04030f,0x08030f,'option f',(Keycode.E,)),
        (5,2): button(0x04030f,0x08030f,'option g',(Keycode.F,)),
        
        #up/down floor
        (4,3): button(0x0a030f,0x0a080f,'Up floor',(Keycode.SHIFT, Keycode.COMMA,)),
        (5,3): button(0x0a030f,0x0a080f,'down floor',(Keycode.SHIFT, Keycode.PERIOD,)),
        
        #Wear/Takeoff armor
        (6,0): button(0x04030f,0x04080f,'wear',(Keycode.W,)),
        (7,0): button(0x04030f,0x04080f,'takeoff',(Keycode.SHIFT, Keycode.T,)),
        
        #puton/remove rings 
        (6,1): button(0x04030f,0x04080f,'put on',(Keycode.SHIFT, Keycode.P,)), 
        (7,1): button(0x04030f,0x04080f,'Remove',(Keycode.SHIFT, Keycode.R,)),
        
        #eat,pickup
        (6,2): button(0x04030f,0x04080f,'eat',(Keycode.E,)),
        (7,2): button(0x04030f,0x04080f,'pickup',(Keycode.COMMA,)),
        
        (6,3): button(0x04030f,0x04080f,'goto',(Keycode.SHIFT, Keycode.MINUS,)),
        (7,3): button(0x04030f,0x04080f,'exchange',(Keycode.X,)),
        
  # (7,3): button(0x04030f,0x04030f,'goto',(Keycode.UNDERSCORE,)),
 }
