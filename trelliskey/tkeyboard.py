'''
reference: https://github.com/adafruit/Adafruit_CircuitPython_HID/blob/master/adafruit_hid/keycode.py
'''
from trelliskey.keycode import Keycode
import time
    
class button:
    '''
    Represents the information associated with a button on trellis.
    Class Attribute:
    _kbd: Contains the low level keyboard implementation that will send HID keys to keyboard.
    '''
 
    _kbd = None
 
    def __init__(self,upcolor,downcolor,desc,keys):
        """
        :upcolor: The color the button will take on when in an up state.
        :downcolor: The color the button will take on when in a down state.
        :desc: Description of the button what is this button doing in the implementation
        :keys: set of keycodes that will be sent to keyboard when this button transitions from up to down.
        """
        self.upcolor = upcolor
        self.downcolor = downcolor
        self.desc = desc
        self.keys = keys

    def downaction(self):
        """
        Send press event with associated keys to the keyboard implementation
        """
        print('down',self.desc)
        print('down',self.keys)
        if self._kbd != None:
            self._kbd.press(*self.keys)

    def upaction(self):
        """
        Send release event with associated keys to the keyboard implementation
        """
        print('up',self.desc)
        if self._kbd != None:
            self._kbd.release(*self.keys)

'''
Abstract class for keyboard defintions
Has action handlers for down and up
Key defintiions reside in the Class variable Keymap which is overridden by subclass
'''
class tkeyboard:
    Keymap = {}

    def __init__(self,kbd,trel):
        """
        Create an instance of the tkeyboard.
        Supply HID keyboard object and trellis object to constuctor.
        keyboard object is used for HID key actions
        trellis is for pixel color feedback
        """

        ##All the buttons share the same HID keyboard object
        button._kbd = kbd
        
        self.trellis = trel
  
        ##Set initial color layout.
        for k,v in self.Keymap.items():
            self.trellis.pixels[k] = v.upcolor

    def pressed_keys(self):
        """
        report the set of keys that are currently pressed
        """
        return set(self.trellis.pressed_keys)

    def do_downaction_set(self, downset):
        """
        For all reported keys that are identified as down, execute the down action
        """
        for k in downset:
            self.do_downaction(k)
   
    def do_downaction(self, downkey):
        """
        Perform down action on one key and update the color of the key
        """
        if downkey in self.Keymap:
            button_info = self.Keymap[downkey]
            button_info.downaction()
            self.trellis.pixels[downkey] = button_info.downcolor 

    def do_upaction_set(self, upset):
        """
        For all reported keys that are now up, execute the upaction
        """
        for k in upset:
            self.do_upaction(k)
     
    def do_upaction(self, upkey):
        """
        Peform the up action on one key and update the color of the key
        """
        if upkey in self.Keymap:
            button_info = self.Keymap[upkey]
            button_info.upaction()
            self.trellis.pixels[upkey] = button_info.upcolor

    def blinkcode(self, key, color):
        """
        Do a quick blink to indicate something
        was hoping for a mask of current value, but apparently current value is not know (not subscriptable)
        """
        before = color | 0xf0e0a0
        for _ in range(5):
            self.trellis.pixels[key] = before
            time.sleep(0.1)
            self.trellis.pixels[key] = color
            time.sleep(0.2)
