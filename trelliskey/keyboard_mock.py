'''
fake trellis keyboard.  Used for simulation when trellis isn't available.
'''
class Keyboard:
    def press(self,*keys):
        print("Sending press event:{}".format([k for k in keys]))

    def release(self,*keys):
        print("Sending release event:{}".format([k for k in keys]))

