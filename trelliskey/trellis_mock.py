import itertools

'''
fake trellis. Used for imulation when trellis isn't available.
'''
class TrellisM4Express:
    '''
    dictionary where key is (row,column) and value is pixel color.
    '''
    
    pressed_keys = [(0,1)]
    
    def __init__(self,rotation):
        self.pixels = {x:0x000000 for x in itertools.product(range(8),range(4))}


